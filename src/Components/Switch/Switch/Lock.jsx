import { Component } from "react";

class Lock extends Component {
  render() {
    return (
      <button onClick={() => this.props.showButtonToLock()}>
        {this.props.children}
      </button>
    );
  }
}

export default Lock;
