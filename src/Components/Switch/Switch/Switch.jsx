import { Component } from "react";

class Switch extends Component {
  render() {
    return (
      <button
        disabled={this.props.locked}
        onClick={() => this.props.showButton()}
      >
        Mostrar Logo
      </button>
    );
  }
}

export default Switch;
