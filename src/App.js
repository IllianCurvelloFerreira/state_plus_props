import logo from "./logo.svg";
import "./App.css";
import { Component } from "react";
import Switch from "./Components/Switch/Switch/Switch";
import Lock from "./Components/Switch/Switch/Lock";

class App extends Component {
  state = {
    showlogo: true,
  };

  state = {
    isLocked: false,
  };

  showButton = () => {
    if (this.state.showlogo !== false) {
      this.setState({ showlogo: false });
    } else {
      this.setState({ showlogo: true });
    }
  };

  showButtonToLock = () => {
    if (this.state.isLocked !== false) {
      this.setState({ isLocked: false });
    } else {
      this.setState({ isLocked: true });
    }
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {this.state.showlogo ? (
            <img src={logo} className="App-logo" alt="logo" />
          ) : null}
          <Lock showButtonToLock={this.showButtonToLock}>
            {this.state.isLocked ? <p>Destravar</p> : <p>Travar</p>}
          </Lock>
          <Switch locked={this.state.isLocked} showButton={this.showButton} />
        </header>
      </div>
    );
  }
}

export default App;
